# Talk - Using .todo files & a second brain to boost developer productivity

Alternative heading -> Using .todo files & a second brain to boost your productivity

How to clarify your thinking & remember everything

How to think clearly & remember everything

A process to think clearly & remember everything

Using a second brain to think clearly & remember everything.

What is a 2nd brain - how to think clearly & remember everything.

What is a 2nd brain or how to think clearly & remember everything.

What is a 2nd brain? ...how to think clearly & remember everything.


## Outline

- Intro & about talk
    - a system that I've used to:
        - improve my productivity
        - basically remember everything
        - seem like a wizard
- caveat: There is no silver bullet
    - now that I've promised all that, I'll take expectations back a bit
    - https://en.wikipedia.org/wiki/No_Silver_Bullet
    - no magic bean / pill that will just make everything better
    - productivity in my exp is dev'd over time ***unfortunately*** through
        - trail + error
        - finding what works best for you
        - ***a lot*** of finding what does not work for you
    - don't view this as that "one thing" you need to do & everything will suddenly change
    - view this as another tool / method in you arsenal that you can use in your work
- some history / how system came about
    - last place I worked at lots of tasks, used JIRA
        - tasks sometimes involved
            - deep investigative work
            - writing documentation
            - debugging
            - gather test data or planning tests
    - context switching -> sometimes going between tasks while code in review
    - writing down info in random places
        - paper, different note pads sometimes
        - word docs
        - .txt files in notepad
    - this wasn't sustainable or only kind of helped
    - problems I encountered
        - forgot what I'd done
        - forgot useful commands that I created or learned
        - loose train of thought forgot what I was doing / what to do next
        - would plan things out in my head & be interrupted / become distracted / forget what I am doing
- a solution
    - note taking was good
        - but was unstructured not & always searchable (if on paper)
        - not versioned
        - noy kept in the same place, spread across variosu dirs
    - so I added some formality & structure to this
        - with a "dev notes" directory
        - kept a particular directory structure with my own naming conventions
        - the structure made it easy to add new content following the pattern
            - I just add another file / dir in the the same naming convention
    - I also versioned the directory using git
    - with git the directory is now
        - versioned
        - back them up
        - diff them if need be
    - now I had a solution to keeping notes organised + structured , I needed a way to clarify my thinking
    - as I mentioned before I could loose my train of thought easily
    - that is where .todo files come in
- where .todo files come in
    - what is .todo file
        - 
    - *show example*
- the system
    - tool set
        - git
        - vscode + .todo plugin
    - directory structure
    - example files with content
```
my-awesome-notes/
├─ scratch.md
├─ todo.todo
├─ key-links.md
├─ knowledge/
│  ├─ docker.md
│  ├─ git.md
│  ├─ other-tool-or-process.md
├─ tasks/
│  ├─ CODE-3456/
│  │  ├─ 3456.todo
│  │  ├─ 3456-notes.md
│  ├─ CODE-2432/
│  │  ├─ 2432.todo
│  │  ├─ 2432-notes.md
├─ month-logs/
│  ├─ jan22.md
│  ├─ feb22.md
```
- recommendations
    - keep dir structure as flat as possible
    - name files appropriately
        - mainly for easy fuzzy search

- some warnings
    - no sensitive information
    - no personal information
    - do you trust your git host?
    - won't handle dates / deadlines / reminders

- summary
    - write stuff down digitally
    - version it
    - mentally unload & break down tasks into units
        - track completion of those tasks
    - declutter regularly
    - does not need to be perfect, discover your structure


## Planning

### Generate ideas

- mention how the pre

---

Story board

begin
- start with some questions about productivity
    - who has been working through the day - and forgot what they were doing
    - who has been interrupted & forgoten what you were doing first
    - who has come to the end of the day & realised you havent completed that key task
- start with a story about your productivity woes
- intro what I want to share with them
    - how to clarify your though processes
    - how to remember important information
middle
- my story / the problems
    - what I did at work / noticed that made things difficult
    - talk throug the process 
    - the problems that I noticed
- a solution / process I created incrementally to tackle problems
    - either 
        - go through each problem & metion how process solves it
        - talk through the process , them at end say how it tackles each problem

end

---

problems that people face:
- interruptions
- forgetting (natural)
- not breaking down tasks (do the task mentality)
- no planning


key things you need:
- nesting / chunking / taskbreak down / subtasks
    - funny picture , step 1 start with circles, step 2 draw the rest of the owl owl 
- note taking / knowledeg capture
- prioritisation
- reminders / due dates
- secure storage / backups
- digitization
- searchable / tags
- unified
- distraction list

---

plan out your bang

* thanks for coming to presentation

* start off with a couple of questions
* feel free to raise hand or just nod if they appy or you've experienced it

* Have you ever came back to working on something after a break / holiday or a weekend and can't remember where you left off?

* Have you ever had a busy day with lot of tasks planned out and found yourself constanly switching but not getting them all complete by the end of the day?

* Have you ever learn a key bit of very useful info that you need again in future but can't remember it

* these are all things I'v experienced myself

* what if I told you about a process you could use to help prevent all of these problems , that's what I'm going to talk to you about today

* by the end of my talk I want to equip with a process to 
    * clarify you thinking 
    * 

* hopefully by the end of my talk you will have some new processes to h
* i'm going to 


---

Questions the audience might as me 
- are there any apps you can recommend for non devs to use & do this process
    - todoist is biggest on I'd reccomend
    - microsoft todo is another but not as good as todoist
- how do you go about prioritising work
    - urgency / important matrix
    - 

---

Storyboard

- Intro / Hi welcome to talk
- start with rehtorical questions
- caveat - no sliver bullet
- story about my productivity woes at work
- ID some of the problems -> say how this is linked to how brain works
- we can solve this with 2nd brain - what is that
- mention somethings that would help with this - "your 2nd brain"
- how do these all tie together in a process & where does 2nd brain come in
    - CORD productivity model
- summary


---

Flesh out 1

- Intro
    - Hi I'm Kevin
    - Going to talk to you today about
    - using a 2nd brain to think clearly & remeber everything!
- Questions
    - start off with some rehtorical Q's 
    - don't have to answer
    - just get you thinking
- Q1
    - have you ever
    - come back to work after break
    - can't remember where you left off
    - weekend , holiday, lunch even
- Q2 
    - have you ever
    - had a busy day
    - lots planned
    - lots of switching tasks
    - but ... not getting all done
- Q3 
    - have you ever
    - learnt a key bit of info
    - forgetten it
    - needed it again some point in future
- What if I told you
    - about process to help prevent problems like these
    - that's what I'll cover today
- Objectives 
    - recognise:
        - our limitations as humans
            - how affects productivity
        - typical productivity problems (caused by our limitations)
            - how concept of 2nd brain can help
    - give you an example process
        - not only way to do this by any means
        - welcome people actively modifying / stealing what suits them

- limitations as humans
    - were only human after all
        - we have inherent limitations
        - but 2 in partiuclar can affect productivity

    - limited memory, we forget things / dates / what we were doing
        - some examples from myself are...
            - will forget what I'm currently doing on a task
                - especially if interrupted
            - will forget that I have something important to follow up on

    - limited concentration, we loose focus / train of though / want to multitask / switch context
        - saying among devs context switching is expensive
        - takes mental effort / energy to change between tasks
        - think of a CD player switching songs
            - if its constantly switching - song never plays

- what can we do about this?
    - this is where 2nd brain concepts comes in
    - what is 2nd brain

    - I define this as using tech to overcome our limitations

    - so we'll use an app
    - or some digital system to assist our memory & help us think clearly

- what are some of the things we want out of our 2nd brain
    - prioritisation - choose & see what is important
    - breaking down tasks - this is what I mean by think clearly
        - want to be able to take massive task and chop it up into steps
    - searchable - so we can find information in future
    - note taking & saving - we should be able to capture information & save it


- so I'll give an overview of the system & process I use then show you it in action
    - so I use:
        - an organised set of folders
        - a text editor with a plugin
        - a rough process

- folder structure
    - top level folder to keep everything called notes
    - tasks folder to keep information on tasks I do
    - a knowlede folder to keep things I've learnt
    - a scratch file to put random thoughts
    - a single .todo file to put everything I need to do inside of

- the process
    - Capture -> Organise -> Do -> Review

        Capture
        - so I start off by capturing every & anything relevant
        - I call this a brain dump , just to capture all thoughts
        - usually at the start of the day or start of new work

        Organise
        - then I start organising this usually by tasks
        - this is where I break things down into steps 
        - if I have enough info on what I'm doing I will prioritise
            - critical , high, low
        - gather any useful information

        Do
        - this is hard bit, protect your concentration by NOT multitasking
            - one thing at a time
        - I'll pick a task / step to work on and see it to completion
            - try to use visual aid here & comeback to this list if I forget / get distracted
        - organise step important here
            - when things are chopped up its easier to follow
        - capture any relevant or useful information in rough note form
        - I just try to go through steps in order I decided earlier

        Review
        - at a particular interval I'll review what I've done & organise outputs
        - put notes in the right folders
        - clean up any notes captured , mark tasks as complete



- demo
    - I'll start sharing my screen to show this in action
    - so I have my text editor open 
        - called VS Code
        
    - you can see my open notes folder 
        - tasks folder
        - knowlede folder
        - scratch file
        - todo file

    - in todo file I have everthing I'm doing

    - broken down into tasks

    - I can fold sections so I can only see & focus on what I'm doing right down

    - I can create projects with tasks & subtasks & complete or cancel them

    - Inbox in where I do my capturing

    - Then I turn them into a project

    - When I'm done with a task like this one I'll create a folder in tasks to put an info there

    - All of the pretty colors from the plugin



- Recap / top tips
    - This is just the way I do this
        - you'd probably have your own way that works for you
        - there are todo apps you could do this in

    - Use a digital system to capture your notes / thoughts and outputs
    - Use a system to break down what you have to do into steps
        - use this time to organise your system
    - Have a rough process that you follow
        -  Mine was , Capture / Organise / Do / Review
        - yours maybe entirely different
    - Find what works for you!











