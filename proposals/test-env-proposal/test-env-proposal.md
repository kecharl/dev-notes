
# Preview Environments Proposal
## Problem Statement
- Product owner does not get to give input / acceptance review before we merge code to develop.
- This makes it harder to fix issues identified by product owner

## Process
* current process
* suggested process

## Solution

### "One liner"

When branches are created spin up a "preview" environment that the PO & Devs can use for testing, once OK'd by the PO the branch is merged and preveiw environment destroyed.

### High Level Solution
- 2 servers,  (could mock on AWS)
	- CI server (Jenkins)
		- optimise for storage
	- App Hosting Server (CapRover)
		-  optimise for Memory
- 2 servers used in test process
	- Dev makes commit to their branch
	- CI server automatically
		- checks out code
		- runs unit & integration tests
		- build container image for CMS web app
			- contain will have
			- .NET app
			- SQL server DB with some mock data
		- pushes container image to registry on AppHost
		- connect to AppHost over ssh & deploy container using cli

Jenkins
Jenkins is an open source automation server. It helps automate the parts of software development related to building, testing, and deploying, facilitating continuous integration and continuous delivery.

CapRover
CapRover is an easy to use app/database deployment & web server manager for your **NodeJS**, **Python**, **PHP**, **ASP.NET**, **Ruby**, **MySQL**, **MongoDB**, **Postgres**, **WordPress** (and etc...) applications!

CapRover helps you increase your productivity by focusing on your apps instead of the underlying infrastructure. Cap Rover is a self hosted alternative to Heroku.

## Potential Issues
- setting up DB in container & populating with data (not done before)
- this only works for CMS , need to think of other solution for Node app