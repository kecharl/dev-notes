# Plan component

## usecase
- grab data for workspace
- display data about workspace
- required fields
	- workspace name
	- workspace contact
	- bot token
	- signing secret
	- channel details
		- channel name
		- slack channel id
- edit all fields
- crud ops on channels
	- includes name & id

## interface (props)
* workspace record
	* name
	* contact
	* token
	* secret
	* channels []
		* name
		* id
		* slack id
* refreshTable

## state
- workspace record state
- 

## validation
- workspace name is alpha numeric
- contact in valid email address
- bot token - char len 57
- sign secret - char len 32



slide
- 11
- 21
- 25
- 28
- 29







 the hill is tough.
 Tap the sap.
 Pull the hull.
 The pal had had it.
 Is the  pup tough?
You: pull the tough pup.
The hut had half of the salt.
You had staff?
The staff sat?
The staff sat.

The hull is tough.
You sat.
Halt: pull it.
Is it half sap?
It is the shaft.

The hull is tough.
You sat.
Halt: pull it.
It is the shaft.

The hull is tough. 
You sat.
Halt: pull it.
It is the shaft.
It is the.
a can with be he she we so to or,,,,.?,,.,.,?

The cat and the pug are up.
Roll to the top.
Rub the soft pug and the cat.
We had to stop the car at the curb.
A crab can cut us?
With a top hat you can be hot.
Huff, puff, and stop.
It is a cut of the rug.

Huff, puff, and start.
Cut up the rag.
Hug a wet rat.
Tug at the straps.
You had a skull.
You are a star of sorts.
Is it a worse cut.

cat is cat
tapped
passed
crass
taps
trapped
trap
trapped
tap
taps
tapped
this
that
this
that
they
the
they
they
this
that
they
what
you'd
you'd

---

Good use of weaving a story & pop culture into your presentation

Great presentation voice, very clear + Good use of statistics , would give more pause time for audience to think over the stats 

WWW: Like the realatable element of the personal story, Great topic feel like I've taken away some great thougts from this, Great interactivity with emoji

EBI: Would consider adding extra slides to breakdown numbered topics


WWW: Good introduction with use of the story + relatable topic

EBI: Reduce the pacing of the story at the beginning

WWW: Great slide deck! Lots of detail on the subject, can tell you are very knowledeble on the area, love the audience interaction

EBI: could start with a bang , maybe some questions to the audience

WWW: Great topic - my eyes have now been opened! + Great use of the structure that we learnt , begining , mid content & recap!

EBI: Not really any presentation issues, sound on video was hard to hear at times

WWW: great natural presentation voice very confident, could do a podcast! Really like the audience interation - made me take notice of my screen time!

EBI: Could add more content, possibly your experience of using Duolingo 

WWW: well done challenging yourself to preset! Good content with a really personal touch , would make a great AND blog post!

EBI: would like to hear more about your experience so far


WWW: I really like the style of the presentation , informative but comfortable. Good use of specific examples of scenarios that audience can relate to. Great question to the audience to check in on them & welcome questioning.


EBI: Time was over but difficult to fit complex topic into fixed time

---


WWW: Great speech, great delivery. Very clear, great pace 
WWW: I looooooooove the topic, I can definitely relate. I’ve learnt over the years to do something similar in terms of digital help and processes for a few reasons. I tried to share with others but never really found the words to convincingly share. I think you’ve just nailed it :-) And I think a lot of people can benefit from a 2nd brain 
EBI: Addition of some imagery could elevate the “text only” slides a bit I think
takarastart to Everyone (14:16)
WWW: Really strong start, calm and confident throughout. Liked that you looped around and linked in how other people can make it their own in the end and made it inclusive.
Florence Beal to Everyone (14:16)
WWW: nice clear delivery, liked the way you used examples from your oen life, loved your name ‘2nd brain’, you are so organised!, clear and concise EBI: more images would be nice
Celisse Saxton to Everyone (14:17)
WWW: Good audience expectation inviting us to think about your questions. Great facial expressions, hand gestures & variation of tone - natural music & dance throughout. Credible knowledge of the topic & articulated/explained well. Loved incorporating a demo. Valuable information to influence the audience to work smarter. Liked your approach by saying ‘find what works for you’(reducing the risk of isolating audience members)


EBI: Longer pausing between slides. If you only have a small amount of text, its better if you can make the font as big as possible (keeping a difference between title & body font size). Maybe a few more images to support on the slides.
 

Time: 10m +

Kevin… WWW: Great start with thought provoking questions and great way to tie it in to the title of the presentation.
WWW: Great pace and delivery. Naturally good presentation voice and style. Good use of personal examples and techniques to take away
EBI: really good to be honest.

WWW: I really like the style of the presentation , informative but comfortable. Good use of specific examples of scenarios that audience can relate to. Great question to the audience to check in on them & welcome questioning.


EBI: Time was over but difficult to fit complex topic into fixed time

---

