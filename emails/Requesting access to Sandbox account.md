  

  

Requesting access to Sandbox account on behalf of team members for AND Engage project.

  

Requesting access for the following users:

[kevin.charles@and.digital](mailto:kevin.charles@and.digital)  (myself)  
[simeon.haigh@and.digital](mailto:simeon.haigh@and.digital)

[munyaradzi.gochera@and.digital](mailto:munyaradzi.gochera@and.digital)

[antony.leons@and.digital](mailto:antony.leons@and.digital)

[tashinga.musanhu@and.digital](mailto:tashinga.musanhu@and.digital)

[drew.pountney@and.digital](mailto:drew.pountney@and.digital)

[simon.piper@and.digital](mailto:simon.piper@and.digital)

  

Please let me know if I can request access for all these users or if I must ask them to request access individually.

  

Many thanks,

Kevin

  

We intend to move the deployment of an internal accelerator project from an ANDi’s personal Azure account to the sandbox Azure account.

  

We want developers to be able to create resources to host the project.  
  
Please see these docs for more background if need be:

-   AND Engage overview, [https://anddigitaltransformation.atlassian.net/wiki/spaces/CLUB/pages/3906797569/AND+Engage](https://anddigitaltransformation.atlassian.net/wiki/spaces/CLUB/pages/3906797569/AND+Engage) 
-   State of deployment / devops (with costings), [https://anddigitaltransformation.atlassian.net/wiki/spaces/CLUB/pages/4069523477/Engage+-+State+of+Deployment+DevOps+-+26+April+2022](https://anddigitaltransformation.atlassian.net/wiki/spaces/CLUB/pages/4069523477/Engage+-+State+of+Deployment+DevOps+-+26+April+2022)