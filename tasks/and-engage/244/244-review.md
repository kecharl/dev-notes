# AE-244 CMS - Games page does not list full list of available games

## Background

This MR fixes a bug introduced during the last merges.

Bug: The games page does not list all of the available games as buttons. 

## Key Changes

**ClientApp/.env.test:**
- test needs its own environment so this .env file was added
    - we set REACT_APP_API_END_POINT to a different port than the dev environment so you can run the devserver & run tests without the port conflict when mocking endpoints during tests

**src/AppRoutes.jsx:**
-  rename of RouterPath entries

**src/AppRoutes.test.jsx:**
-  add tests for AppRoutes.jsx

**src/api.js:**
- add GameData to ApiEndpoints object

**FilterableTable/QuizTable.jsx:**
-  fix warning from react related to keys
- auto formatted

**statCards/StatCards.js:**
- refactor StatCards to use named ApiEndpoints object & use global axios insance

**config/routerPaths.js:**
- rename RouterPath entries to be more consistent / descriptive

**pages/Games.jsx:**
- add missing buttons to Games page component 
- render buttons using map

**pages/Games.test.jsx:**
-  add tests for Games.jsx

**testing/constants.js:**
-  add new entries to MockApiEndpoints object

**testing/mockRequestHandlers.js:**
-  add named mock request handlers for use with mock service worker library
- intended to make it easy to reuse request handlers across different tests



## Limitations
- was not able to add automated testing of buttons on game page
    - got errors such as `Error: connect ECONNREFUSED 127.0.0.1:8888` & `Network Error` even when mocking `useNavigate`
    - was not able to find a cause so test was removed

## References

N/A