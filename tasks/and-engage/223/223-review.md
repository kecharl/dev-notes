# AE-223 Ability To View Quiz Questions

## Background

This MR add the ability to list, add, edit & delete Quiz question for the quiz game for ANDEngage.

## Key Changes

**ClientApp/.env**
- added REACT_APP_API_END_POINT to default .env file so the value is used as the endpoint when unit tests are run

**ClientApp/.npmrc**
- add config to enforce node / npm version setting in package.json

**ClientApp/.prettierrc**
- file to configure prettier tool to auto format code

**ClientApp/jsconfig.json**
- Added support for absolute imports
    - https://create-react-app.dev/docs/importing-a-component/#absolute-imports
    - https://code.visualstudio.com/docs/languages/jsconfig#_jsconfig-options

**ClientApp/package-lock.json**
**ClientApp/package.json**
- remove usage of tilde & caret in package.json so we must explicitly decide to update a package
- set minimum require version for node & npm
- added type definitions using jsdoc

**src/api.js**
- added named objects with constants for Api endpoint names

**Modal/Modal.js**
- update close button for component, for improved accessibility

**FilterableTable/FilterableTable.js**
- data fetching method in Filterable table, data fetching function now passed to child DataTable component, allows a DataTable to update the data in it indirectly

**FilterableTable/FilterableTable.test.jsx**
**FilterableTable/QuizTable.test.jsx**
- added test for these components

**FilterableTable/MugOfDestinyTable.js**
**WorkspaceTable/WorkspacesTable.js**
- fix class name warning
- switch to absolute imports
- auto format file

**src/config/routerPaths.js**
- added named object containing router path names

**src/index.css**
- added new class `visually-hidden` to hide elements used for accessibility descriptions 

**src/AppRoutes.js**
**src/AppRoutes.jsx**
**src/pages/Games.js**
**src/pages/Games.jsx**
- renamed from .js -> .jsx

**src/components/organisms/FilterableTable/QuizTable.jsx**
**src/pages/QuizGame.jsx**
- added new file for Quiz Game

**src/testing/constants.js**
**src/testing/helperComponents.jsx**
**src/testing/mockDatabaseRecords.js**
- added files to help woth testing
    - defined constants object
    - helper components for test environment setup
    - mock data for testing

**src/types/databaseRecords.js**
- added type definitions using jsdoc
    - https://jsdoc.app/

**EngageCMS/README.md**
- added to document setup process for project


## Limitations

- still some areas with accessibility issues

## References

* https://docs.npmjs.com/cli/v8/using-npm/config#engine-strict
* https://docs.npmjs.com/cli/v7/configuring-npm/package-json#engines
* https://code.visualstudio.com/docs/languages/jsconfig
* https://create-react-app.dev/docs/importing-a-component/#absolute-imports
* https://code.visualstudio.com/docs/languages/jsconfig#_jsconfig-options
* https://mswjs.io/docs/
* https://jsdoc.app/

cmsGamesPageDoesNotListFullListOfAvailableGames