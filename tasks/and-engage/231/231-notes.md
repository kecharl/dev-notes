# AND Engage Current Dev Ops Setup

This doc summarizes the suite of services the AND Engage project uses to deploy & the resources the project uses when deployed.

Currently the app is hosted across Azure & GCP:

- an AND organizational account
- an ANDi's personal Azure account

We intend to unify this by hosting everything on the AND Azure Sandbox account.

Below is a breakdown of what & where we are using is. For the resources in GCP , information on what we will migrate to in azure will be listed

## High level solution

The high-level solution consist of 2 parts & can be seen in the diagram below:

- CMS
  - a .NET Core app that uses Entity framework to interact with a Microsoft SQL Server database
  - there is a React frontend used to manage the content in the database
- Slack App
  - a Node JS app which makes use of SDKs from slack to run the actual games in channels
  - data about the game state is persisted to a Firebase datastore

-- put image here --

![deployments-image](blob:https://anddigitaltransformation.atlassian.net/fb30eba4-6e1b-498e-8ef2-fbe5946f08bc#media-blob-url=true&id=3c5719de-681b-4120-b806-af006e67916d&collection=contentId-4054351887&contextId=4054351887&mimeType=image%2Fpng&name=AND%20Engage%20HL%20architecture.png&size=115014&height=741&width=1221&alt=)

## Azure

The CMS is hosted in Azure, we make use of the Azure DevOps suite of tools - specifically we only make use of the Pipelines service.

It should be noted that we make use of the **free tier** of the Azure Devops suite which limits us to 5 free users.

We intend to rotate the users who have access to the devops suite when necessary if possible.

See details about DevOps pricing here: https://azure.microsoft.com/en-us/pricing/details/devops/azure-devops-services/

### Environments

For the project we have 3 environments, Development, User Acceptance Testing & Production.

We have organized our resources in Azure according to this using 3 resource groups:

- EngageCMS_DEV
- EngageCMS_UAT
- EngageCMS_PROD

Resources belonging to a group are tagged with the appropriate group.

See this confluence doc for more details about the environments: https://anddigitaltransformation.atlassian.net/wiki/spaces/CLUB/pages/4054516092/Engage+Environments

### Pipelines

We have 3 established pipelines, one for each environment:

- Engage CMS - Develop (Build & Test)
  - for development resources e.g. dev database
- Engage CMS - UAT
  - for deployment to the UAT environment (staging)
- Engage CMS - PROD
  - for deployment into production


The UAT & Prod pipelines have the following identical steps:

- Install nuget 6.0.0
- Restore nuget packages
- Build .NET solution
- npm install (react app deps)
- npm build react app
- Copy built react app (to WebAppContent folder for .NET build)
- Archive built solution (into .zip
- Run tests for project (not hooked up yet)
- Publish build artifacts (to Azure pipelines)
- Create & Configure Azure Web App
- Deploy Azure Web App

Each of the steps in a pipeline & the pipeline as a whole has associated YAML than can be exported to help with migration.

### Triggers

The dev pipeline is triggered on commits to the `develop` branch.

The UAT pipeline is triggered on creation of any branch under `release/*`.

The Prod pipeline in triggered on commits to the `master` branch.



### c

Here is a breakdown of the resources each environment uses & any associated cost - the database server is shared across environments.

#### Database

In Azure an SQL Server instance can be created that can hosts multiple SQL Databases.

So we have 1 server `engage-cms-db` with 3 SQL databases

- `engage-cms-dev`
- `engage-cms-uat-db`
- `engage-cms-uat-db`

We are charged for each SQL database being used with the SQL Server.

_Note: The Server instance `engage-cms-db` & databases are tagged as being in the `EngageCMS_DEV` but the databases are used by their respective environments._

#### Dev Environment

Type, Name, Pricing Tier, Est. Monthly cost, Pricing link

- SQL server ,`engage-cms-db`, N/A, N/A
- SQL database , `engage-cmd-dev`, DTU Basic, 6.23 USD, https://azure.microsoft.com/en-gb/pricing/details/azure-sql-database/single/#pricing
- Key Vault ,`engange-cms-dev-kv`, standard, 0.03 USD, https://azure.microsoft.com/en-gb/pricing/details/key-vault/#pricing
- Storage account ,`engagecmsdev`, Standard (GPv2) storage - Hot, 0.019 USD, https://azure.microsoft.com/en-gb/pricing/details/storage/blobs/

#### UAT Environment

- SQL database , `engage-cmd-uat`, DTU Basic, 6.23 USD,
  /single/#pricing
- App Service Plan, `enagage-cms-uat-sp`, Free Tier, 0.00 USD, https://azure.microsoft.com/en-gb/pricing/details/app-service/windows/#pricing
- App Service, `enage-cms-uat-app`, N/A, N/A
- Application Insights, `engage-cms-uat-app`, Pay-As-You-Go, 0.00 USD, https://azure.microsoft.com/en-gb/pricing/details/monitor/
- Key Vault, `engange-cms-uat-kv`, 0.03 USD, https://azure.microsoft.com/en-gb/pricing/details/key-vault/#pricing
- Storage account, `engagecmsuat`, Standard (GPv2) storage - Hot, 0.019 USD, https://azure.microsoft.com/en-gb/pricing/details/storage/blobs/

#### Prod Environment

- SQL database , `engage-cmd-prod`, DTU Basic, 6.23 USD, https://azure.microsoft.com/en-gb/pricing/details/azure-sql-database/single/#pricing
- App Service Plan, `enagage-cms-prod-sp`, Free Tier, 0.00 USD, https://azure.microsoft.com/en-gb/pricing/details/app-service/windows/#pricing
- App Service, `enage-cms-prod-app`, N/A, N/A
- Application Insights, `engage-cms-prod-app`,
- Key Vault, `engange-cms-prod-kv`, 0.03 USD, https://azure.microsoft.com/en-gb/pricing/details/key-vault/#pricing
- Storage account, `engagecmsprod`, Standard (GPv2) storage - Hot, 0.019 USD, https://azure.microsoft.com/en-gb/pricing/details/storage/blobs/

#### Cost summary

- SQL database, 18.69 USD
- App Service Plan, 0.00 USD
- App Service, N/A
- Application Insights, 0.00 USD
- Key Vault, 0.09 USD
- Storage account, 0.057 USD

Total: 18.837 USD


### Migration plan

Our plan is to keep the same setup we have from the ANDi's Azure account to the AND Sandbox account, then take some time to assess the deployment setup & consider any alternative approaches.

When we make any plans we have to change the solution architecture we will create a proposal doc of the new solution with a costings estimate & present to a tech lead / practice team for review.


## Google Cloud Platform

The Slack App is hosted in GCP, at its core is a Node JS process connected to a cloud datastore.

The resources differ from Azure & are more loosely structured due to the original MVP & its data being placed there.

### Environments

As mentioned the AND Engage project has 3 environments, Development, User Acceptance Testing & Production.

- There is no resources deployed in GCP for the DEV environment, dev testing is done locally
- There are resources in GCP for a UAT deployment
- There are resources in GCP for a PROD deployment

The main GCP service used is Cloud Run.

### Pipelines

We use the Pipelines CI/CD feature in Gitlab to manage deployments of the Node JS project.

In Gitlab pipelines are added into the source code of the project with an associated YAML file, the file is in our project is:
https://gitlab.com/ANDigital/bootcamps/andengage/-/blob/main/.gitlab-ci.yml

The pipeline has 4 stages (3 in active use)
- test
    - runs 
- build (inactive)
- staging
- deploy

#### build stage


### Triggers

### Resources & Costing

Cloud Build 

Cloud Run

Cloud Datastore

Cloud Logging

Cloud Firestore

Cloud Scheduler

Cloud Pub/Sub

Cloud Resource Manager

