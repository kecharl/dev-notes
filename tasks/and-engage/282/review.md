# CMS: When creating a new Guess where, location defaults to Huddersfield

## Description

 MR fixes bug that causes the edit modal when editing a Guess Where Game record to always select the first location in the dropdown - even thougth the location stored with the record is different.

This also fixes 2 other issues:
* Answer to Guess where question is not displayed in the table
* Navigating to index.html causes 404 error
 
## Cause & fix

The bug was caused by the `defaultValue` not being set for the `select` element used for the choice of location. This is now set to the location stored with the record being edited.