- aws training kevin / tash 1. days
- Simeon afternoon off 

move https://anddigitaltransformation.atlassian.net/browse/AE-331 to backlog

this ticket should be 

https://anddigitaltransformation.atlassian.net/browse/AE-335
* add integration environment for shared deployed - preview
* keep names short , dev , int, uat, prod

https://anddigitaltransformation.atlassian.net/browse/AE-336
- keep shared sql instance
- don't focus on tear up & tear down unless we have time
	- prefer shared resources


https://anddigitaltransformation.atlassian.net/browse/AE-338
* put the instance in the prod group and share the resources


we want to create a dummy document of what to do if it break
- devs
- links to resources
- what to do / how to do it

https://anddigitaltransformation.atlassian.net/browse/AE-289
* update ticket to update entity to track montly score 

update knowledge transfer plan - create tasks on the sprint



# Chat about Sky
- 


# Background
This MR:
- fixes the bug which causes a 404 error when a page other than the index page is refreshed
- Adds support for using GNU Make targets to for easier testing & development of the CMS
- Added the Microsoft template for Single Page Applications to the `EngageCMS.Web.csproj` , this make is easier to develop the react frontend
- Clarified what the environment names are in the .NET project
- Adds dev, uat & prod profiles to the `launchSettings.json` file
# Analysis

## 404 Bug
* Refreshing the CMS site on any page other than the index page causes a 404 error
* This is caused by the .NET app not finding these pages in `wwwroot`, these paths are virtual & all navigation is done client side
* The solution was to make the .NET app serve up the `index.html` page for any requests that are not API requests as a fallback, it is last in the middleware chain so it should not be sent as a response so should not affect requests to the REST API
* The react app is built as part of the publish MSBuild process and the static files put in `wwwroot` folder so the .NET app can find it
* Once the `index.html` file is served the navigation is handled client side by react router

## Make targets
- A Makefile has been added with targets to do the following
	- run a development server locally 
	- build a docker image with the app installed
	- run a docker image with the app installed
- The dev server is used to easily work on the CMS & frontend in a change / test workflow, 
	- the web page will auto refresh on code changes 
	- the .NET app should rebuild when files are changed
- The docker images are meant to emulate how the app will be hosted in production
	- this allows devs to test features such as routing locally in a reliable manner
	- this can also be used by the product owner to do local testing before deployment
	  
## Microsoft SPA template
- Microsoft provide a template for building SPA apps 
	- This template adds some config to the project file:
		- config to auto launch a dev server for our Create React App (called the Microsoft.AspNetCore.SpaProxy)
		- using this server gives us hot module reloading where the page refreshes if changes are made to the source code
	- In production this SpaProxy is not used & the static `index.html` file is served from `wwwroot`
## Clarification of environments
- to comply with the standard environment methods in .NET (eg `isDevelopment() / isProduction`) 
	- `Production` is used instead of `prod` for the `ASPNETCORE_ENVIRONMENT` variable
	- `Development` is used instead of `dev` for the `ASPNETCORE_ENVIRONMENT` variable
- This stops us having to write custom middleware to match these specific text values (`dev / prod`)
- `uat` is still used for the `ASPNETCORE_ENVIRONMENT` variable , we have a custom middleware `isUAT()` to detect it
- You can see is variables being inserted via the Makefile

## launchSettings.json
- Launch profiles files were added for each of our environments
	- EngageCMS-Development , for dev environment
	- EngageCMS-UAT , for uat environment
	- EngageCMS-Production , for prod environment
- The launch profiles make it easier to run a development server from the command line and give preconfigured targets to run when using visual studio
- The launch profiles preconfigure settings for each environment

# Changes

# Limitations