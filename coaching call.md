network chuck

find out the details of the stuff you want to do for your coaching session

---

# What stuff do you want to do / achieve?
- eventually run small tech business
	- probably some sort of graphical / design tool  / editor
- want to put most focus on becoming front end expert
	- currently know mostly the backend of the frontend (state management, fetching data, browser APIs)
- want to know enough about the backend to be useful or "dangerous"
	- eg how to make a secure CRUD api that accesses a database would make me happy
- really interested in 3D / graphics / rendering
- frontend focal areas
	- get better at CSS / design
	- web apps over websites
	- canvas
	- WebGL
	- know more about key html
		- tables
		- forms
		- a11y
- pick up rust at somepoint - seems like it will be useful

--- 

- dig into how sky ui works
- canvas stuff could be used in potential accelerator app
- slack wellness app
- CapRover talk / write up blog post
- have a outcome or goal realated to your learning goals
- you have to make use of the knowleged

## goal for next session
- come up with a list of core front end stuff to learn with an order
- using https://www.frontendpractice.com/projects , I will learn layout css fundamentals
- outline for a talk on caprover

long term goal of building and rendering the system you build with 

interactive card deck game with multiplayer

consider 80/20 split for work skills & interests
	- do some research into 3d market 

skills matrix - P&P
	- try to get project to line up with missing skills in matrix

goals
	- carry on with css work
	- have a look at skills matrix, have a feel for where you are placed within it
	- have a think about what kind of project you would want to work on

 Presession prep
 - have a list of front end stuff to remake (going project based)
 - have ideas of what to build 2 things I find interesting
	- nginx frontend
	- flashcards web app
- thought about the 80 / 20 split for work & intrests , think I found something practical
- 

- look into visualisation in learninging time
	- stats about contant stact stuff
	- links ect
	-

- look into HBDI profile for getting opinions and thoughts and bring it to the table
	- confidence & contributing in meeting
		- make sure you know  your opinions are valid in meetings
			- 	we all deserve to be there, dont be afraid to speak up
		- spend some time doing pre meeting prep

- dev forum in sky, try to get some topics to lead part of discssion
	- testing month - get involved in automated testing at sky and can give a talk on that
	- scrum vs kanban discussion