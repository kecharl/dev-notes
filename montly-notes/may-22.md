
--- 

# May 23 / 2022

Engage: Developer Hand Over Manual

This doc is intended to be used as a manual for any Developer who is picking up the AND Engage project after it was paused. 

In this document you will find:
- information about development processes we use currently
- links to repositories & cloud provider accounts
- links to contact information of previous team members
- technical debt or processes we intended to improve

Note this document covers the project form a technical / developers perspective , you will want to consult with the Product Owner & this doc https://anddigitaltransformation.atlassian.net/wiki/spaces/CLUB/pages/4083974163/Engage+PO+Start+here

There may be product goals that are more important than some of the technical changes we wanted to make.

## Introduction

The AND Engage project is a Slack bot that allows users in a Slack channel to play interactive mini-games with one another, there is a competitive element where users accumulate a score by playing games.

The solution is made up of 3 parts:
- Slackbot
	- a NodeJS app that runs scheduled "Jobs" these are the games that are posted in the Slack channel
	- the app communicates with Slack in real time over a Websocket to take input from players
- CMS Backend
	- a .NET core application that provides a REST api for manipulating data in the CMS
	- it uses Entity framework for interaction with the database
- CMS Frontend
	- a client side rendered React application that Administrators can access to modify data in the CMS
	- it uses the REST API of the CMS backend

Take a look at this document https://anddigitaltransformation.atlassian.net/wiki/spaces/CLUB/pages/4054351887/Engage+solution+overview, for a high-level overview of the architecture.

The Slackbot is deployed to Google Cloud Platform.

The CMS Backend is deployed to Microsoft Azure, & the frontend is static so it is served by the .NET app running the backend.

## Important Links
### Contact information

Contact details for previous team members can be found here: https://anddigitaltransformation.atlassian.net/wiki/spaces/CLUB/pages/4054482959/Engage+Start+here#Previous

### Development Repositories
## Development Workflows

### Branching Methods
We currently 

### Performing a Release

See this document on how a release is prepared for AND Engage: https://anddigitaltransformation.atlassian.net/wiki/spaces/CLUB/pages/4103733285/Engage+How+to+perform+a+release


Outline
- About engage from developers perspective
	- quick summary about product
	- summary about technology used
	- quick summary about hosting solutions used
- link to help / phone book page with description
- Development workflow
	- branching method
	- reviews
	- testing
	- release methodology
- Things we would like to change / move to
	- 12 factor app
	- branching -> trunk based dev
	- preview environments for Product Owner


--- 

# 24 May 2022
- quiz game works
- mug of destiny works
- movie scene not working
- guess where not working

--- 

# 25 May 2022

An exception occurred while polling the repository. Error: Microsoft.TeamFoundation.Build2.Server.Extensions.ExternalConnectorException: Found invalid data while decoding. ---> System.IO.InvalidDataException: Found invalid data while decoding. at System.Net.GZipWrapperStream.EndRead(IAsyncResult asyncResult) at System.Net.Http.HttpClientHandler.WebExceptionWrapperStream.EndRead(IAsyncResult asyncResult) at System.Net.Http.StreamToStreamCopy.StartRead() --- End of stack trace from previous location where exception was thrown --- at System.Runtime.ExceptionServices.ExceptionDispatchInfo.Throw() at System.Runtime.CompilerServices.TaskAwaiter.HandleNonSuccessAndDebuggerNotification(Task task) at Microsoft.VisualStudio.Services.Common.TaskCancellationExtensions.<>c__DisplayClass1_0.<<EnforceCancellation>b__0>d.MoveNext() in D:\a\_work\1\s\Vssf\Client\Common\TaskCancellationExtensions.cs:line 27 --- End of stack trace from previous location where exception was thrown --- at System.Runtime.ExceptionServices.ExceptionDispatchInfo.Throw() at System.Runtime.CompilerServices.TaskAwaiter.HandleNonSuccessAndDebuggerNotification(Task task) at Microsoft.VisualStudio.Services.Common.TaskCancellationExtensions.<EnforceCancellation>d__2`1.MoveNext() in D:\a\_work\1\s\Vssf\Client\Common\TaskCancellationExtensions.cs:line 49 --- End of stack trace from previous location where exception was thrown --- at System.Runtime.ExceptionServices.ExceptionDispatchInfo.Throw() at System.Runtime.CompilerServices.TaskAwaiter.HandleNonSuccessAndDebuggerNotification(Task task) at Microsoft.VisualStudio.Services.Common.VssHttpMessageHandler.<BufferResponseContentAsync>d__19.MoveNext() in D:\a\_work\1\s\Vssf\Client\Common\VssHttpMessageHandler.cs:line 464 --- End of stack trace from previous location where exception was thrown --- at System.Runtime.ExceptionServices.ExceptionDispatchInfo.Throw() at Microsoft.VisualStudio.Services.Common.VssHttpMessageHandler.<SendAsync>d__17.MoveNext() in D:\a\_work\1\s\Vssf\Client\Common\VssHttpMessageHandler.cs:line 268 --- End of stack trace from previous location where exception was thrown --- at System.Runtime.ExceptionServices.ExceptionDispatchInfo.Throw() at Microsoft.VisualStudio.Services.Common.VssHttpRetryMessageHandler.<SendAsync>d__9.MoveNext() in D:\a\_work\1\s\Vssf\Client\Common\VssHttpRetryMessageHandler.cs:line 148 --- End of stack trace from previous location where exception was thrown --- at System.Runtime.ExceptionServices.ExceptionDispatchInfo.Throw() at Microsoft.TeamFoundation.Build2.Server.Extensions.GitConnector.GetBranches(IVssRequestContext requestContext, ExternalConnection connection, Int32 timeoutSeconds, Boolean useAnonymousAccess) in D:\a\_work\1\s\Tfs\Service\Build2\Extensions\SourceProviders\Git\GitConnector.cs:line 0 --- End of inner exception stack trace --- at Microsoft.TeamFoundation.Build2.Server.Extensions.GitConnector.GetBranches(IVssRequestContext requestContext, ExternalConnection connection, Int32 timeoutSeconds, Boolean useAnonymousAccess) in D:\a\_work\1\s\Tfs\Service\Build2\Extensions\SourceProviders\Git\GitConnector.cs:line 144 at Microsoft.TeamFoundation.Build2.Server.Extensions.GitSourceProvider.GetMatchingBranchRefs(IVssRequestContext requestContext, BuildDefinition definition, IList`1 branchFilters) in D:\a\_work\1\s\Tfs\Service\Build2\Extensions\SourceProviders\Git\GitSourceProvider.cs:line 463 at Microsoft.TeamFoundation.Build2.Server.Extensions.GitSourceProvider.GetSourceVersionsToBuild(IVssRequestContext requestContext, BuildDefinition definition, List`1 branchFilters, Boolean batchChanges, String previousVersionEvaluated, Dictionary`2& ciData, String& lastVersionEvaluated) in D:\a\_work\1\s\Tfs\Service\Build2\Extensions\SourceProviders\Git\GitSourceProvider.cs:line 369 at Microsoft.TeamFoundation.Build2.Server.Extensions.BuildPollingJobExtension.Run(IVssRequestContext requestContext, TeamFoundationJobDefinition jobDefinition, DateTime queueTime, String& resultMessage) in D:\a\_work\1\s\Tfs\Service\Build2\Extensions\BuildPollingJobExtension.cs:line 98.