# 1 June 22


## What is AND Engage 

## Technology 
In the technology section we'll start off by giving an overview of the AND Engage solution architecture , then take a deep dive into some of the specific components in more detail.

## Architecture

### Slide 

- 2 Main Components
	- CMS - content creation / editing
	- Node App - runs scheduled games
	  
- CMS - 2 parts
	- Frontend, CSR React Application
	- Backend, REST API - .NET C#

```
Right lets kick this off with a quick overview

the AND Engage solution is made up of 2 main components
- a CMS, which admins use to create and edit content
and
- a Node App, which runs scheduled games in a specified Slack Channel

the CMS can be broken down into 2 parts
- the frontend, which is a client side rendered react application
	- this is the UI admins will use to add or edit content in the CMS
- and the second part is the backend, which provides a REST API for the front end and Node App to use
	- this is a dot net core application written in C sharp

Now we'll step into a more detailed view of how exactly these 2 components work in production
```



### Slide 
```
So this is the highlevel solution architecture digram, don't worry I'll step through it piece by piece but one of the first things you can see is...
```

### Slide 

### Slide 

## Build System


- went well overall
- chat and interaction went well I think, had good rapport with Sr Dev & Scrum Master
- think they liked:
	- the fact that I said I can pickup anything and get cracking
	- breadth of tech I have experienced
	- 
- he mentioned he wondered if the ammount of work might feel overwhelming since 
- 