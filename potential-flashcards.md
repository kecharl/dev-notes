
F

B
`#STKPWHRAO*EUFRPBLGTSDZ`

`# STKPWHRAO * EUFRPBLGTSDZ`

---

F
Use this hook to store state needed for rendering
B
useState

F
What is `useState` hook used for?
B
To store state needed for rendering

---

F
Use this hook to reference a value not needed for rendering
B
useRef

F
What is the useRef hook used for
B
To reference a value not needed for rendering

---

F
How do you get all elements to use alternative box model?
B
- set `box-sizing` property on html element
- set all other elements to inherit that value

```
html {
  box-sizing: border-box;
}
*, *::before, *::after {
  box-sizing: inherit;
}
```

---

F
What is margin collapse?
B
When two element's vertical margins touch, the margins are combined.


---

F
Margin collapse: what happens when both margins are positive?
B
- bigger margin becomes the margin between the elements.

---

F
Margin collapse: what happens when  both margins are negative?
B
- bigger margin becomes the margin between the elements. 
- Since this is a negative value the elements will overlap.

---

F
Margin collapse: what happens when  one margin is negative and the other positive?
B
values are added to produce the combined margin.

---

F
Margin collapse: what happens when a `<br/>` tag is between two elements? 
B
- The margins are blocked from collapsing.
- Only adjacent elements collapse.

---

F (front & back card)
Tag used to markup a: **paragraph**

B
`<p></p>`

Used to markup?

---

F (front & back card)
Tag used to markup a: **heading**

B
`<h1><h1/>`  , h1 -> h6 can be used

Used to markup?

---

F
How should header tags (`<h1>`) be organised?
B
Heading should be organised hierarchically.

Used to markup?

---

F (front & back card)
Tag used to **visually italicise** text.

B
`<i><i/>`

Used to do what?

--- 


F (front & back card)
Tag used to **verbally emphasise** text.

B
`<em><em/>`

Used to do what?


---

F (front & back card)
Tag used to markup a: **xxx**

B
`<></>`

Used to markup?

---

F (front & back card)
Tag used to markup a: **xxx**

B
`<></>`

Used to markup?

---

F (front & back card)
Tag used to markup a: **xxx**

B
`<></>`

Used to markup?

---

F (front & back card)
Tag used to markup a: **xxx**

B
`<></>`

Used to markup?

---
