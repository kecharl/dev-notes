https://aws.amazon.com/economics
https://aws.amazon.com/solutions/case-studies
https://aws.amazon.com/free
https://aws.amazon.com/aws-cost-management/aws-budgets
https://calculator.aws
https://aws.amazon.com/partners/training/path-tech-pro/
Migrating to AWS Technical
Security Best Practices
[https://explore.skillbuilder.aws](https://explore.skillbuilder.aws)


# AWS Certifier Cloud Practitioner

## 1 - Introduction to AWS
- 

## 2 - AWS Compute
- secure resizable compute capacity
- boot server instances in minutes
  
- instance types
	- general purpose
		- balances compute memory and networking resources
		- suitable for a bread range of workloads
	- compute optimised
		- offers high performance processors
		- ideal for compute intensive applications and batch processing workloads
	- memory optimised
		- delivers fast performance for memory intensive workloads
		- well suited for high perf databases
	- accelerated computing
		- uses hardware accelerators to expedite data processing
	- storage optimised
		- data warehouse
		- offers low laten
		  
- EC2 pricing
	- on demand
		- no upfront cost or min contract
		- pay by the hour
		- ideal for short term irregular workloads
	- spot 
		- spare capacity of aws
		- upto 90% cheaper
		- only for workloads that can be interrupted
		- ideal for workloads with flexible start and end times
		- offers savings over on demand prices
	- reserved 
		- upto 72 % discount to on demand
		- requires a 1 or 3 year contract
			  
* EC2 auto scaling
	* scale capacity as computing requirements change
	* use dynamic scaling and predictive scaling
	  
* Elastic loadbalaning 
	* automatically distributes traffic across multiple resources
	* provides single point of contact for your auto scaling group
	* load balancing
	  
  - serverless compute services
	  - aws lambda
	  - run code without provisioning or managing servers
	  - pay only for compute resources that are running
	  - use other AWS services to automatically trigger code
	  
- AWS container services
	- Amazon ECS, Elastic container services
	- Amazon EKS, Elastic Kubernetes Service
	- both are container orchestration services 
	  
- AWS Fargate
	- run serverless containers with Amazon EKS or ECS

- Messaging Services
	- Amazon Simple Notification Service (SNS)
		- messages published to topics
		- subscribers immediately receive messages for their topics
		- mostly used for monitoring and alerting
		- can be used for push notifications
	
	- Amazon Simple Queue Service (SQS)
		- send, store & receive messages between software components
		- queue messages without requiring other services to be available
		  
		  
		  appreciate if this is too technical , but how much data can something like SQS store ? Like the is does the message contain data or just the message and the data held elsewhere like S3

## AWS Global Infrastructure and Reliability

- region is independent, private, nothing is shared to other regions
- resources are not exposed to internet by default this must be configured
- regions have 3 physical / logical availability zones
- some services are deployed by default across multiple AZs
- regions are highly available and fault tolerant

- seleting a region
	- compliance with data governance and legal requirements
	- proximity to you customers
	- available services within a region
	- pricing
	
* global content delivery
	* **Amazon CloudFront** delivers content
		* **edge locations** have caches

- AWS Outposts
	- extend AWS infrastructure and services to your on premises data center
	
- Interacting with AWS services
	- AWS management console
	- CLI, command line interface
	- SDK, software development kit


## AWS Networking

- VPC , Amazon virtual private cloud
	- enables you to launch resources in a **virtual network** that you define
	  
- Subnet - subnetwork , is a section in a VPC in which you can place groups of isolated resources
	- a subnet can be public or private
		- public resources can be accessed via the internet
		-  private resources cannot be seen or accessed via the internet, only accessed via resources in the VPC
- VPC is closed to access from the internet
- to make VPC accessible from the internet we need to add an internet gateway
- Virtual private gateway - internet gateway for private traffic from a private datacenter via a VPN connection
  
- AWS Direct connect
	- direct , private and dedicated connection between on premises to AWS
	- literally a private Fiber connection
	- large bandwidth
	  
- Network Access Control List (NACL)
	- virtual firewall to control traffic to a subnet
	- in-front of a subnet
	- stateless packet filtering
		- apply only logic and rules from firewall - does not remember packets / requests
	- before a packet can exit a subnet it must be checked against he outbound rules
	  
- Security Group
	- virtual firewall for an EC2 instance to control traffic
	- in-front of on instance
	- stateful packet filtering
		- apply logic and rules from firewall - remember packets / requests
	- remembers previous decisions that were made for incoming packets
	- all instances have a SG, not possible to NOT have one with an instance
	- by default a SG blocks all traffic 
	
- DNS
	- route 53 is service we use to route users to services in AWS

## AWS Storage

* Block storage, EBS - Elastic Block storage
	* block volumes that can be attached to instances
	* if terminated the EBS volume is removed by default
* File Storage, EFS - Elastic file system (Linux) / FSX (Windows)
	* network share or network attached storage
	* store data in a scalable file system
	* provide data to thousands of Amazon EC2 instanced concurrently
	* based on NFS
	* store data in and across multiple availability zones
* Object storage, Simple Storage Service (S3)
	* in object storage each object consists of data, metadata and a key
	* store object in buckets 
	* set permissions to control access to objects 
	* choose from a range of storage classes for different use cases
	
	* S3 storage classes
		* S3 standard
			* **designed for frequent access**
			* min 3 availability zones
		* S3 standard infrequent access
			* **ideal for infrequently accessed data**
			* similar to standard but lower storage price & higher retrieval price
			* min 3 availability zones
		* S3 One Zone infrequent access
			* same as infrequent access but only in 1 AZ
		* S3 intelligent tiering 
			* ideal for data with unknown access frequency
			* requires small monthly monitoring fee
		* S3 Glacier
			* low cost for data archiving
			* **able to retrieve objects within a few min to hrs**
		* S3 Glacier Deep Archive
			* lowest cost object storage class
			* able to retrieve objects within 12 hours

## AWS Databases
- SQL & No SQL
- Relational Database
	- AWS RDS , Relational Database Service
		- managed service
		- operate & scale relational database in the cloud (scaling is self managed)
		- automate time consuming & admin tasks
		- store & transmit data securely
	- Engines
		- AWS Aruara
		- PostgreSQL
		- MySQL
		- Oracle DB
		- Microsoft SQL Server
		- Maria DB
- Amazon DynamoDB is a serverless key value database
	- automatically scales to adjust  for capacity changes and maintain consistent performance
	- it is designed to handle over 10 trillion requests per day


## AWS Security
* shared responsibility model
* 
* customers responsible for data IN the cloud
	* operating system
	* applications
	* security groups
	* host based firewalls
	* account management
	  
  - IAM , identity and access management
	  - allows you to manage access to AWS services and resources
	  - IAM user 
		  - an identity that represents a person or app that interacts with AWS services and resources
	  - IAM policies
		  - a doc that grants or denies permissions to AWS services or resources
		  - best practice: principle of least privilege
	  - IAM groups
		  - collection of IAM users
		  - best practice: attach IAM policies to IAM groups, rather than to individual IAM users
		  - members inherit the policies assigned to the group
	  - IAM role
		  - is an identity that you can assume to gain temporary access to permissions
		  - avoids sharing credentials between users
		  - will expire after fixed amount of time - from mins to 36 hours
	  - AWS Organisations
		  - helps customers consolidate and **manage multiple AWS accounts in a central location**
		  - use service control polices to centrally control permissions for the accounts in your orgs
		    
- Compliance
	- AWS Artifact
		- provides on demand access to security and compliance reports and select online agreements
		- review and manage agreements signed with AWS
		  
- Application security
	- AWS WAF , Web Application Firewall
		- 1st line of defense for infrastructure in AWS
		- filter traffic before they reach AWS resources
		- able to open & inspect packets 
		- can create own rules
		- can subscribe to AWS created rules
		  
	- AWS shield
	- provides protection against distributed denial of service attacks
	- free & comes out of the box
		- DoS / DDoS Attack
			- overwhelming resources with malicious requests so legitimate traffic does not get a response
			- DoS - single attack sources
			- DDoS - multiple attack sources
			  
	- Amazon Guard Duty
		- provides intelligent threat detection for AWS products and services
		- needs to be enabled
		- continuously analyses network and account activity
		- intelligently detects threats
		- review detailed findings and take action

## Monitoring an Analytics
- Amazon CloudWatch
	- collect metrics from all services you use in AWS (application and resources / performance)
	- display the metrics you want in multiple customisable dashboards
	- monitor your AWS and on premises infrastructure and resources in real time
	- access all of your metrics from a single location
	- configure automatic alerts and actions in response to metrics
	  
- AWS CloudTrail
	- **track user activities** and API requests throughout you AWS infrastructure
	- audit trail for tracing actions
	- all actions in AWS is logged
	- filter logs generated by API calls to assist with operational analysis and troubleshooting
	- auto detect unusual activity
	  
- AWS Trusted Advisor
	- receive real time guidance for improving your AWS environment
	- compare your infrastructure to AWS best practices
	- service is tied to support plan
	- security advice as free

##  Billing
- Consolidated billing
	- receive single bill for all the AWS accounts in your organisation
	- share saving across the accounts in the organisation
	- allows billing to be applied to the organisation to take advantage of volume based discounts

- AWS Cost explorer
	- a tool that you can use to visualise and understand you AWS costs and usage over time
	- can be used to forecast costs up-to 1 year in the future

- AWS Support Plans
	- basic
		- free to all AWS customers
		- technical papers
		- documentations
		- AWS Personal Health Dashboard
		- limited selection of AWS Trusted Advisor checks
	- Developer $
		- cheapest premium
	- Business $ $
		- mid tier price premium
		- all AWS Trusted Advisor checks are available from the business support plan & up
	- Enterprise $ $ $
		- most expensive premium


## Migration & Innovation

- migration strategies
	- Rehost
		- move infrastructure from on-prem to cloud
		- lift & shift
	- Re-platform
		- move to managed services
		- no application impact
		- save staff time with reduced ops / management / admin tasks
	- Refactor
		- rework application to work with cloud native services (eg Lambda)
		- 
	- Repurchase
		- use AWS marketplace to buy third party infra / services in the cloud
	- Retire
		- don't keep applications or infra , get rid of them
	- Retain
		- resources / applications are not migrated - cost reasons or no business need

- AWS Snow Family
	- Snowcone
		- small rugged secure edge computing & data transfer device
	- Snowball
		- AWS Snowball Edge storage optimized 
	- Snowmobile
		- exabyte scale data transfer service for moving large amounts of data to AWS
		- up-to 100 PB data
		- physically transfer data to AWS

- Well Architected framework
	- recommendation , helps you understand how to design and operate reliable secure efficient and cost effective systems in the AWS Cloud
	- 5 pillars
		- Operational excellence
		- Security
		- Reliability
		- Performance efficiency
		- Cost Optimisation

### Technical AWS path
