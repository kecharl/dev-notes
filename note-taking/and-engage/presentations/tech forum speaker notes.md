

# Architecture

Right lets kick this off with a quick overview

the AND Engage solution is made up of 2 main components
- a CMS, which admins use to create and edit content
and
- a Node App, which runs scheduled games in a specified Slack Channel
  

the CMS can be broken down into 2 parts
- the frontend, which is a client side rendered react application
	- this is the UI admins will use to manage content in the CMS
- and the second part is the backend, which provides a REST API for the front end and Node App to use


Now we'll step into a more detailed view of how exactly these 2 components work in production

## -----------------------------------------

So this is the high level solution architecture diagram

There’s a fair bit going on here - so I'll step through it piece by piece but one of the first things you can see is

The deployment is across 2 cloud providers

## -----------------------------------------

The CMS is deployed in Azure

## -----------------------------------------

The Node App is deployed in Google's Cloud platform

## -----------------------------------------

Now I will give you a breakdown of each component then describe the interactions to paint a picture of how it all works.

## -----------------------------------------

First up we have the Client device, this is what an Admin will use to access the CMS

We support both desktop and mobile experiences

## -----------------------------------------
Next - We have the Key Vault

This is a service for storing cryptographic keys and other secrets used by cloud workloads.

We use it to store connection strings for the Storage Account & Database that the service uses
## -----------------------------------------
Then - We have the App Service Instance

This is a PaaS offering in Azure for hosting web applications, REST API's and backend services.

We deploy our .NET core application to this service and we get:
- static hosting of our frontend content , as this is served up via our .NET app 
- we also get our REST API for performing C R U D operations in the CMS
## -----------------------------------------

The next component is the Storage account.

This is a service providing different forms of storage for Cloud workloads.

We only make use of a blob storage container - this is also known as object storage in other cloud vendors

This is where images that are uploaded to the CMS are saved

## -----------------------------------------

Next up we have an SQL Server instance, with a number of databases - in the diagram here we are only showing the Production Database.

This is a managed database service in Azure & in this database we store content added to the CMS

## -----------------------------------------

The Node App has less moving parts, 

We start off with the Slack Client, this is what players use to play AND Engage games in a specific channel

## -----------------------------------------

Next up we have a cloud run instance.

This is a PaaS offering in GCP for hosting contanerized applications.

We build a docker image in which the Node app runs and this image is deployed to Cloud Run.

## -----------------------------------------

Finally we have a  Datastore.

This is a GCP service providing a NoSQL database for cloud workloads.

We use the datastore to store the game state & leaderboards

# Interactions

Now I'll take you through the interactions between components quickly

We'll go through the CMS first,

## -----------------------------------------

To begin with the App Service spins up

## -----------------------------------------

It access the KeyVault to get the connection strings for database & the storage account

## -----------------------------------------

An admin visits the web page , the .NET app running in App Service serves up the static index.html page that loads the client

## -----------------------------------------

The client uses the REST API to get & display CMS records

## -----------------------------------------

The CMS records are read from the Database


## -----------------------------------------
If the user accesses any records with images, content is either retrieved or sent to the blob container
## -----------------------------------------

Next on to the Node app interaction, 

We start off with our cloud run instance starting up and launching a game at a particular schedule

## -----------------------------------------

It uses the REST API of the CMS to get content that is used in the game

## -----------------------------------------

It establishes a WebSocket connection to communicate with slack to run the game

Over this socket we get input from users & update the UI

## -----------------------------------------
As the game is running and when the game is finished game state & leaderboard data is stored in the data store by our Cloud run app
## -----------------------------------------

# Tech Deep Dive
In this section we'll dive a bit deeper into the tech we use in the project:

We'll go over the:
- build system
- CMS Backend
- CMS Frontend
- Node Application
- Testing
## -----------------------------------------
## -----------------------------------------
## -----------------------------------------
## -----------------------------------------
## -----------------------------------------
## -----------------------------------------
