https://docs.google.com/presentation/d/11hk83ejWflsBnMw1XYJbVLf5WzzvTKzZbjP057VAdC8/edit#slide=id.p

High everyone thanks for coming to the accelerator catch up meeting


We have another quick presentation for you today - we'll follow a similar format to last week 

admitedly not a lot has changed so apologies to those that we're in the last update and you see a lot of repeated stuff

-   Give an update about what we’ve been doing since the last update including a demo
-   Cover what were working on now
-   What we plan to work on next
-   And finally take any questions / have a discussion

Back to me covering what we will work on next






I guess one thing that's worth mentioning is 
that the current product order Drew is going to be
rolling off onto client so I'll be stepping into the 
Product Owner roll until either I am placed with a client 
or someone with a PA background joins us whose skills might
benefit the roll a bit better

so I'll be having a few handover sessions next week with Drew

So if we could just open up the link to our Roadmap

from a high level we can see that we're in the middle of the current roadmap for what we have in mind as the overall product vision

the current focus for the team is
* engagement drive and
* commercialisation
* techical debt reduction / bug resolution

I'll give you some details about what these all actually mean

we'll start with in engagement drive - in one sentence this is us trying to see if we can grow engagement with the AND engageme app in slack
- this comes from us Drew doing some data analysis on our engagement metrics we've been tracking and using this data to come up with some hypothesis that we want to test
- the idea being that each hypothesis we confirm or reject will lead us closer to growing the engagement metrics and hopefully building a better product for our users
- so an example of this hypothesis testing is we would measure the engagement of a particular game modify the length of time that game is played for and observe if this increases engagement
- and we'd keep these little experiments isolated so they don't influence one another
- so this is something that's going to be a slow burn in the project so maybe a ticket or 2 towards it each sprint & we'll collect and analyse our metrics


Commercialisation
* so one thing that came out of the last update is that there was no objection to us pepsunig a path where we build this product in a way that it could be commercialised if we wanted to
* reason behind that being it's something we could role out to clients to help with organisational culture change
* from a technical perspective it gives team members a chance to experiece doing work that is less glamorous compared to adding new features but is important for making enterprise level software - agian experience that will hopfully help when on client
* so the first step in that for us in to add user access and management so this sprint we're modelling and thinking about how exacty that will work and we'll just keep chugging along with that
* so the next natural step is once we have a IAM model we'll look to get logins working 
* then next would likely be access control based on roles in the CMS

Finally we have the on going areas of technical debt reduction & bug resolution
* so I think what we've done so far is to aim for 20% of the sprint work to be dedicated to this
* and from our discussions and retrospective we view bug resolution to be linked to some of our tech debt related to testing
* because we don't have any tests really past unit tests the And Engage components we find that we are getting some bugs slipping through and its taking away time to work on our main sprint goals
* so the big target for technical debt reduction is to significantly increase the coverage we have of test - integration and end to end tests
* and make tests part of our pipeline - so the team had a useful session with Ali & Sam who give input & design help with what our desired pipelines will be & testing will be a big part of that
* another thing we want to do is have a look at making it easier for the Product owner to review changes for acceptance criteria before work makes it into the shared develop branch


Question:
* as part of commecialisation we'd need to role this out to different orgs - how do we architect a solution for this


- stress testing
- security testing
- channel sizing
- more commercial 
- you can do this quickly
- use wider and org as a test bed
- need to think about on board flow
	- quick start for free
- andengage premade content for free
- challenge: think about how and engage gets paused
	- repriortise what needs doing before a pause