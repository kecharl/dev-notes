# Monthly Tech Forum (June 6): AND Engage

## Outline

- what is AND Engage briefly
- technology
	- architecture
		- create & show new diagram, with overall description of service
	- tech break down
		- build system
			- make / msbuild / docker
		- CMS Backend
			- pick interesting bit of code to show
		- CMS Frontend
			- pick interesting bit of code to show
		- Node Application
			- pick interesting bit of code to show
- processes
	- branching
	- performing a release / deployment
- challenges / things we wanted to do / improve
	- increase testing coverage because of X
	- move to trunk based branching because of X
	- establish preview environments because of X
		- bugs merged into branch not met acceptance criteria



# check which branch you are on 
EngageCMS git:(feature/AE-122-example-task) ~ git status
On branch feature/AE-122-example-task

# checkout new branch from feature branch
EngageCMS git:(feature/AE-122-example-task) ~ git checkout -b integration/AE-122-example-task

# now on new branch
EngageCMS git:(integration/AE-122-example-task) ~ git status
On branch integration/AE-122-example-task

# push branch upto Gitlab
EngageCMS git:(integration/AE-122-example-task) ~ git push -u origin integration/AE-122-example-task