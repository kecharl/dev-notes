# Migrate to AND Azure

## Merge changes in CMS Merge Request 144 into a migration branch - adding support for hosting the Frontend static files with the CMS
-  This Merge request adds support for hosting the front and static files using the cms .NET Application, this means that the application can be served from a single port on one server, fixes the refresh bug and adds a number of improvements for testing locally using docker
- This should be merged when the move is made to the AND Azure account as it would break the current deployment pipelines
- https://gitlab.com/ANDigital/adams/accelerator/andengage-cms/-/merge_requests/144

### Acceptance Criteria
- The code should be reviewed before being merged
- A branch should be created for code changes related to the migration, it should branch from the latest develop
	- call it: migrate-to-and-azure
- The branch is merged successfully

### Points 1

## Overhaul codebase to use terms , development, staging, production
- We have decided on the terms development staging & production for our environments we need to change any references to Dev Prod and UAT to the following
	- dev -> development
	- uat -> staging
	- prod -> production
- It is recommended that this task is done at the end of the migration process as to not have conflicts that may break the develop branch
### Acceptance Criteria
- References to dev, uat & prod in the CMS .NET codebases have been updated
- References to dev, uat & prod in the React Frontend have been updated
- References to dev, uat & prod in the Node App have been updated
- Debug & Release should be the available configurations for building the .NET CMS - all other options should be removed
- ClientApp/environments/ directory for the React application is replaced with conventional  environments for a "create react app" application
	- ClientApp/environments/.dev.env -> ClientApp/.env.development
	- ClientApp/environments/.uat.env -> ClientApp/.env.staging
	- ClientApp/environments/.prod.env -> ClientApp/.env.production
	- see this page for guidance: https://create-react-app.dev/docs/adding-custom-environment-variables/#what-other-env-files-can-be-used
	- https://create-react-app.dev/docs/deployment/#customizing-environment-variables-for-arbitrary-build-environments
	- The scripts in package.json should updated to use the correct environment terminology and load the correct environment file
		- remove these scripts (which will be uneccessary)
			- "start:dev"
			- "build:dev"
			- "start:uat"
			- "build:uat"
			- "start:prod"
			- "build:prod"
		- Add this script
			- "build:staging": "env-cmd -f .env.staging npm run build"

### Points 1
## Find out what the estimated cost of the service running on Azure will be
- we need to find out if there is an update on the cost or if we need to work this out and if this is a blocker for us beginning any work
- We need find out if there is any update on the costing estimate (from Ali), if we need to complete a costing estimate ourselves or if the costing will be a blocker
- We should consider using some of the cost estimation tools that are available from Azure
### Acceptance Criteria
- We have a cost estimate of how much the service will cost to run for each month
- We we know if not having an accurate estimate will block any work

## Find out how we can store secrects / keys in Azure securely & extract any hardcoded secrets / values that should be stored in environment variables
- Currently there are a number of secrets and other  potentially sensitive information  that should be extracted into environment variables hardcoded into the source code
- We need to remove these values from the source code when they are changed in the migration process and place them in environment variables
- Because some of these values store sensitive keys or other sensitive information we need to find out what Azure service we can use to protect it

Currently there are pieces of sensitive information that are check into source control in environment variables. This crew potential security issue.

We need to find out what service is provided by the store these secret values that can be injected into the environment when we perform a deployment.

We also need to create a plan of how we will make use of this service to store and inject the secrets into our deployment environment.

### Acceptance Criteria
- We know which service we can use to store secret information and how to setup that service
- The pieces of sensitive information that are checked into the source code are identified and their locations noted in this document
	- https://anddigitaltransformation.atlassian.net/wiki/spaces/CLUB/pages/4094885972/Locations+in+source+code+of+potentially+sensitive+information+or+information+that+be+should+be+extracted
- We have a documented plan (detailed bullet points will suffice) of how the secrets will be stored and how we will access them when the service is deployed, this can be added to this document
	- https://anddigitaltransformation.atlassian.net/wiki/spaces/CLUB/pages/4095475778/Plan+-+How+to+use+Azure+service+to+store+secrets

## Create SQL server instance & SQL databases for development, staging & production environments
- A core part of our tech stack is a Microsoft SQL Server database, as part of the migration we need to create an SQL server instance in the AND Azure account
- This server should share the 3 databases (1 per environment) just like in our current implementation
- We need also create the tables in each of these databases
	- this means we should manually run the migrations that we have created in the.net application so we get the same tables we have in our current implementation (James' Azure account)
	 - Do not go about setting up automated migrations this should be done in the DevOps pipeline work
### Acceptance Criteria
- A Microsoft SQL Server instance has been created that can be shared across environmentss
- 3 SQL databases in the SQL server instance
	- 1 for development environment
	- 1 for staging environment
	- 1 for production environment
-  The tables in the databases from the existing account should be recreated in the AND Azure account

## Migrate data from SQL tables in James J's account to AND Azure account
- The task AE-338 created the SQL server, databases and tables we need for the CMS service, we now need to migrate the data we have from our existing tables in Jame's Azure account to the AND Azure account.

Feel free to use this document to plan & track this:

[https://anddigitaltransformation.atlassian.net/wiki/spaces/CLUB/pages/4096655367/Data+Migration+Tracking](https://anddigitaltransformation.atlassian.net/wiki/spaces/CLUB/pages/4096655367/Data+Migration+Tracking)

### Acceptance Criteria
- All the records from each table in the existing account are successfully transferred to the database tables in the AND Azure account

## Investigate how the blob storage is created & works
- We are unsure of how exactly the blob storage accounts used in our solution are created, we need to understand how the blob storage account is created and how it is accessed so we can successfully migrate to the AND Azure Account
- Find out if we manually need to create the storage accounts or if they are created as part of the Devops pipeline

### Acceptance Criteria
- we understand how the storage account are created
- We understand how the storage accounts are accessed by our application
- Document briefly in this document https://anddigitaltransformation.atlassian.net/wiki/spaces/CLUB/pages/4096557087/Investigation+-+How+the+blob+storage+is+created+works
- 
## Recreate blob storage accounts in AND Azure account
-  A part of the tech stack is object storage (Blob storage in Azure), we use it to store the images uploaded to the CMS and refer to them via URLs stored in the database tables.
- Using the information gathered in task AE-347 , recreate the blob storage accounts in the AND Azure account

### Acceptance Criteria
- We have a storage account for each deployment environment
	- development 
	- staging
	- Production

## Migrate blob storage data from James J's account to AND Azure account
- The task AE-340 blob storage accounts for each deployment environment
- we need to migrate the image data we have in the existing blob storage account to the AND Azure account
- A key part of this is that the URLs for the images uploaded to the new storage account will certainly be different than the URLs they had assigned to them in the existing account
	- We will need to update the records that reffer to these images in the database to use these new URLs
	- It is likey we will have to create a custom script to do this

### Acceptance Criteria
- The images from the old storage account are all migrated to the new Azure storage account
- Records in the database tables are updated to use the new URLs for these images

## Create naming convention for AND engage resources document this in Ops section in wiki & create a resource group for AND Engage resources
 - We need a naming convention based off of the AND resource name convention document that we can use to give names to all of the resources associated with the AND engage project
 - We need to also come up with a resource group name and create the group name in Azure - this should be at to the wiki page
 - We should do the naming convention on this page then place it in an appropriate place in the wiki 
	 - https://anddigitaltransformation.atlassian.net/wiki/spaces/CLUB/pages/4096557107/Naming+convention+for+AND+Engage+resources+in+AND+Azure+account
### Acceptance Criteria
- We have documented the naming convention in the Wiki page
- We have created a resource group name and had this document on the wiki page
-  we have created the resource group in the AND Azure account
- The document produced is moved to an appropriate place on the wiki

## Explore azure devops offering and decide concrete pipeline based on doc "Proposed environment and build pipelines"
- We have an existing plan for our what desired pipeline would be
- We need to take a look at what is on offer in the Azure services and decide what the concrete pipeline we will actually use
- We should try to stick to the plan as much as possible
- If we need to deviate from the plan create an additional section in the document explaining what the actual pipeline is or how it differs from the plan
- We should plan out how exactly each stage of the pipeline should be done in bullet points
### Acceptance Criteria
- Open understanding of what we can do in the pipeline with services on offer
- We updated the pipeline plan if necessary
- Add a section to the devils pipeline page bullet pointing how each stage of the pipeline will work / what will be done

## Implement build & deployment pipeline based on decision & docs from in AE-343
- AE-343 , created a concrete plan for the DevOps pipeline take this plan and the knowledge gathered and implement the deployment pipeline.
### Acceptance Criteria
- We have a deployment pipeline that can deploy the app to each environment
	- Development
	- Staging
	- Production

## Find out if Gitlab's changes to their free tier offering will affect us & how 
- Gitlab will be making changes to their free plan that will affect our usage of their pipelines
- The changes will affect how much build minutes frree tier projects can use
- we make use of the lab CI / CD features
- we need to find if we will be affected by these changes
- If we are hearted we should document what these changes will mean for us in the wiki.
- NOTE: this is not for deciding what actions should be taken
 - https://about.gitlab.com/pricing/faq-efficient-free-tier/#public-projects-on-gitlab-saas-free-tier
 - https://about.gitlab.com/blog/2021/11/11/public-project-minute-limits/
### Acceptance Criteria
- we have an understanding of what kind of Gitlab account we have 
- We have a standing of what the changes to get lives service will mean to us

## Identify activities required to complete migration from GCP to Azure services
- in JIRA

# Grow AND Engagement for users
## Change leaderboard to reset monthly
## Change leaderboard to show Top 3



--- 
We have
- Until 27th realistically
	- june bank holiday
	- we may start on client that week 30th
	- training
	- maybe 7.5 working days