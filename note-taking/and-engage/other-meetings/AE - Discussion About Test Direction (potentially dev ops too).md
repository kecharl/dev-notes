# Engage - Discussion About Test Direction , Wed 4 May 2022

see: https://anddigitaltransformation.atlassian.net/browse/AE-243

# Notes AE-243

## Components - state of testing
- slack node app
	- node js
	- jest to implement unit testing
	- coverage 13 / 34 files 
	- types of test we currently have
		- unit
	- 42 tests total

- cms frontend
	- react single page app
	- Jest tent runner with react testing library
	- types of test we currently have
		- unit
	- 

- api
	- entity framework core .NET
	- Nunit + MOQ (mocking framework)
	- 8 test files
	- 10% coverage of functionality
	-  types of test we currently have
		- unit
	- we want
		- get access to test-server, microsoft tool
		- tests related to authentication
		- in memory testes
		- integration tests from api to database
		- run these tests as part of the build & deployment
			- integration tests
				- we dont want all of these to run in build / might be slow
				- integration tests should focus on happy path (to not slow down build or pipeline)
				- add all unit tests to part of build job
		- cypress tests using gurkin syntax - as a tool to create tests for what the PO wants
		- where do we want to run tests
			- can run tests locally
		- accessibility tooling - wave
		- static analysis tooling, sonar cube, vericode

## Key points
* get the coverage in and focus on local testing
	* then we can focus on where in the pipeline we want to put these tests
* make sure all tests pass with no warnings
	* node tests - "A worker process has failed to exit gracefully and has been force exited. This is likely caused by tests leaking due to improper teardown. Try running with --detectOpenHandles to find leaks. Active timers can also cause this, ensure that .unref() was called on them."
* add the tests for the CMS frontend to the pipeline
* investigate to see if there are tools we could use to auto make tests on file save

## Tasks identified from discussion
* Make sure all the tests pass without warnins / issues
* Update workflow proccess to document that we should run all tests locally before submission
* Increase coverage of unit tests for CMS app
* Increase coverage of integration tests for CMS app
* ~~Define all environments that we want~~
* ~~Define the pipelines we want for Slack App - technology agnostic~~
* ~~Define the pipelines we want for CMS App - technology agnostic~~
* Setup the ability to run end to end tests using Cypress
* Migrate to Adams Azure Account
* Figure out what Azure costing will be for And Engage when moved to AND Account
* Create a more detailed breakdown of how the Pipelines for CMS / Slack App pipelines
	* tear up / down process will be keys
* Consider how databases will be handled in our pipeline processes


## Session on Tasks

### Task: Define all environments that we want

#### Desired CMS environments
- local dev
	- local branch + developers machine + remote SQL server
- dev (manual)
	- feature branch, we want to deploy to shared test env from branch for PO review before merging to develop branch
	- develop branch, can be deployed so PO can test changes as a whole
- staging (automatic)
	- release / hotfix branches, we want this to be  auto deployed when we create new branches under release/x hotfix/x
- production (automatic)
	- main branch, we want this to be  auto deployed  on release being mered into branch

#### Desired Slack App environments
- local dev
	- local branch + developers machine + + scoket mode
- dev (manual process)
	- feature branch, we want to deploy to shared test env from branch for PO review before merging to develop branch
	- develop branch, can be deployed so PO can test changes as a whole
- staging (automatic process)
	- release / hotfix branches, we want this to be  auto deployed when we create new branches under release/x hotfix/x
- production (automatic process)
	- main branch, we want this to be  auto deployed  on release being mered into branch

#### Desired Pipeline 

What we want the pipe line to involve:
1. Build App
2. Run .NET unit tests
	1. Fail - Fail pipeline
3. Run JS tests (jest)
	1. Fail - fail the build
4. Deploy to slot / test application
5. Run Cypress tests
	1. Fail - fail the pipeline
6. Swap slot or deploy

- we would do steps 1 - 3 on each commit / push
- we need to think about which steps will be run for each environment
- Need to think about how we will do alerts

