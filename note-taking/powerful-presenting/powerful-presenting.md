

Plan
- have to make audience care, presentation not only about information transfer
- purpose , where are the audience coming from , where are you moving them to
- where is it taking place
- who are the audience
    - size, knowledge, why attending, name / role

Create 
    - the big idea
        - title
        - mind map
    - content design
        - storyboard
         -direction
         audience 
         content
         format
    - practice

    What makes a great title
        - address your specific audience
            - avoid being vague
        - highligt the specific benefit or outcome they desire
        - higihlihgt the specicfic pain they most want to avoud
        - create curiosity
        - add urgency

    Storyboard

    beginnig
        - tell them what youre going to tell them
        - greeting warm up
        - bang - attention grabber know your key messageso
        - tell a story
        - start with the end in mind
        - intro & objectives
    middle
        - less in more
        - rule of 3
        - repetition
        - use stats that shock
            - 18 min attention span
        - put into context
        - tell them
        - content
    end
        - tell them what youve told them
        - summarise key / messages
        - remind of purpose / aim 
        - benefits - make personal
        - so what , what next
        - questions
        - summary


    process
        - generate ideas
        - cluster
        - create messages
        - order messages

    direction
        think about the order of the content & how to bring the audience along
        - chronological
        - sequential - process or step by step
        - climatic - build to something important

Deliver

---




really liked the opening question , gets audience involved
possbily could use something interative like a poll for the quesiton

