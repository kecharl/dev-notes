
What are we trying to achieve with Web App Security?
- confidentiality
- integrity
- availability
- accountability
- non reputation
- assurance
- authenticity


What's involved in Web App Security?

Snyk dependency checking
- https://github.com/snyk/
- https://snyk.io/

---

## Day 2

[(4) Cryptography: Crash Course Computer Science #33 - YouTube](https://www.youtube.com/watch?v=jhXCTbFnK8o)

Content Security Policy

- XSS attack
- Content-Security-Policy http header
	- defa
- https://content-security-policy.com/

Cross Origin Resource Sharing (CORS)

CSRF attack
- user credential harvesting from another site

Cross Origin Resource Blocking (CORB)
