eesa gave me overview
- mono repo
- team city build system
- jira
- scrumban
- graph ql
- koa js

- mention of a junior engineer to mentor / upskill
- hit ground running

- whats biggest challenge your team is facing 

# Sun Setting of All Time Leaderboard

@here Hi AND Engage players

We've made the decision to stop having a regularly posted All Time Leaderboard in favour of a leaderboard for each month. We hope this encourages more players to join in.

You'll see a leaderboard with the top 3 players & you can expand the list to see all score.



